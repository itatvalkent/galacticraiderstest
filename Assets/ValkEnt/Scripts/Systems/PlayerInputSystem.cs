﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class PlayerInputSystem : ComponentSystem
{
	public struct Data
	{
		public readonly int Length;
		public EntityArray entities;
		public ComponentDataArray<Player> players;
		public ComponentDataArray<Heading> headings;
	}

	[Inject]
	private Data data;

	protected override void OnUpdate()
	{
		bool isLeft = Input.GetKey(KeyCode.A);
		for(int i = 0; i < data.Length; ++i)
		{
			var currEntity = data.entities[i];
			if(isLeft)
			{
				var newHeading = new Heading(){ Value = Vector3.left};
				PostUpdateCommands.SetComponent(currEntity, newHeading);
			}
		}
	}
}
