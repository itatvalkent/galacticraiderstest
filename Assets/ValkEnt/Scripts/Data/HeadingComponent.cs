﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public struct Heading : IComponentData
{
    public float3 Value;
}

public class HeadingComponent : ComponentDataWrapper<Heading>
{

}
